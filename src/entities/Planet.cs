namespace Entities
{
	using Godot;
	using System;
	using View.PlanetView;
	using View.PlanetView.Structures;
	using StrategicResources;
	using System.Collections.Generic;

	[Tool]
    public partial class Planet : OrbitingEntity 
	{
		private int _buildingIdCounter = 0;
		private AStar3D _astar = new();
		private readonly Dictionary<long, PlanetBuilding> _buildings = new();
		private readonly Dictionary<long, PlanetTransportRoute> _transportRoutes = new();

        public override void _Ready()
        {
            base._Ready();
        }
        
		public void RegisterBuilding(PlanetBuilding building)
		{
			if (_buildingIdCounter == IdHelper.PLANET_STRUCTURE_MAX)
			{
				GD.PrintErr($"[ERROR] Too many buildings on Planet {Id}");
			}
			building.Id = Id * IdHelper.PLANET_MULTIPLICATOR + ++_buildingIdCounter;
			GD.Print(building.Id);
			if (_astar.HasPoint(building.Id))
			{
				GD.Print($"[ERROR] Added point to AStar3D that already exists in {Name}");
			}
			switch(building)
			{
				case PlanetTransportHub hub:
					hub.Build();
					break;
				case PlanetConstructionBuilding constructionBuilding:
					constructionBuilding.Connect("ConstructionCompleted", new Callable(this, "OnConstructionCompleted"), (int) ConnectFlags.OneShot);
					break;
				case PlanetEconomyBuilding economyBuilding:
					economyBuilding.Connect("ProducedResource", new Callable(this, "OnBuildingProducedResource"));
					economyBuilding.Build();
					break;
				case PlanetBuilding planetBuilding:
					planetBuilding.Build();
					break;
			}
			building.Connect("Destroyed", new Callable(this, "OnStructureDestroyed"));
			_astar.AddPoint(building.Id, new Vector3(), 1);
			_buildings.Add(building.Id, building);
		}

		public void RegisterTransportRoute(PlanetTransportRoute route)
		{
			route.Id = Id * IdHelper.PLANET_MULTIPLICATOR + ++_buildingIdCounter;
			if (_astar.HasPoint(route.Id))
			{
				GD.Print($"[ERROR] Added point to AStar3D that already exists in {Name}");
			}
			_astar.AddPoint(route.Id, new Vector3(), route.Length / route.TransportSpeed + 1); // + 1 because weight needs to be larger than one
			_astar.ConnectPoints(route.Start.Id, route.Id);
			_astar.ConnectPoints(route.Id, route.Ending.Id);
			route.Connect("Destroyed", new Callable(this, "OnStructureDestroyed"));
			_transportRoutes.Add(route.Id, route);
		}

		public void RouteResource(int startId, StrategicResource resource)
		{
			// Find the building that needs the output resource the most.
			bool excludeWarehouses = false; // Prevent warehouses from rapidly exchanging their stock.
			if (_buildings.ContainsKey(startId) && _buildings[startId] is PlanetWarehouse)
			{
				excludeWarehouses = true;
			}
			PlanetEconomyBuilding receivingBuilding  = FindEconomyBuildingMostInNeed(startId, resource, excludeWarehouses);
			if (receivingBuilding == null)
			{
				return;
			}
			int receivingBuildingFreeCapacity = receivingBuilding.GetFreeResourceCapacity(resource.ResourceName);
			// Make sure to not clog transport routes with empty containers.
			if (receivingBuildingFreeCapacity == 0)
			{
				return;
			}

			StrategicResource transportedResource = (StrategicResource) resource.Duplicate();
			transportedResource.Amount = Mathf.Min(transportedResource.Amount, receivingBuildingFreeCapacity);
			// Reduce the amount of stored resource in the producing building.
			resource.Amount -= transportedResource.Amount;
			// Reserve storage space in the receiving building.
			if (!receivingBuilding.InboundResourcesAmount.ContainsKey(resource.ResourceName))
			{
				receivingBuilding.InboundResourcesAmount[resource.ResourceName] = 0;
			}
			receivingBuilding.InboundResourcesAmount[resource.ResourceName] += transportedResource.Amount;
			// Get the path the resource will travel
			long[] path = _astar.GetIdPath(startId, receivingBuilding.Id);
			// Convert to a list of nodes.
			List<PlanetStructure> pathNodes = GetPathNodes(path);
			pathNodes.RemoveAt(0);
			foreach (PlanetStructure node in pathNodes)
			{
				node.Connect("Destroyed", new Callable(transportedResource, "OnResourceInterimDestinationLost"));
			}
			transportedResource.Path3D = pathNodes;
			transportedResource.Connect("InterimDestinationLost", new Callable(this, "OnResourceInterimDestinationLost"));
			transportedResource.Connect("DestinationLost", new Callable(this, "OnResourceDestinationLost"));
			// Give the resource to the transport route.
			_transportRoutes[path[1]].ReceiveResource(transportedResource);
		}

		/// <summary>
		/// Find the production building that is the most in need of the provided resource.
		/// </summary>
		/// <param name="fromBuilding"></param>
		/// <param name="resource"></param>
		/// <returns>PlanetProductionBuilding if one has been found, otherwise <see langword="null"/>.</returns>
		public PlanetEconomyBuilding FindEconomyBuildingMostInNeed(int fromNodeId, StrategicResource resource, bool excludeWarehouses = false)
		{
			List<PlanetEconomyBuilding> productionBuildings = GetEconomyBuildings();
			if (productionBuildings.Count == 0)
			{
				return null;
			}
			PlanetEconomyBuilding buildingMostInNeed = null;
			foreach (PlanetEconomyBuilding planetBuilding in productionBuildings)
			{
				if (!IsInstanceValid(planetBuilding) || planetBuilding.Id == -1 || planetBuilding is PlanetTransportHub || planetBuilding is PlanetWarehouse) // Warehouses are only taken into consideration if every building is full
				{
					continue;
				}
				foreach (string buildingResourceName in planetBuilding.StoredInputResources.Keys)
				{
					if (buildingResourceName == resource.ResourceName)
					{
						if (_astar.GetIdPath(fromNodeId, planetBuilding.Id).Length <= 1) // This is going to make everything slow probably.
						{
							break; // Break out of iterating over resources.
						}
						else if (buildingMostInNeed == null)
						{
							buildingMostInNeed = planetBuilding;
							break;
						}
						// Check if the resource is in the dictionaries of the last building and calculate the free capacity for it.
						int lastBuildingResourceCapacity = buildingMostInNeed.GetFreeResourceCapacity(resource.ResourceName);
						// Check if the resource is in the dictionaries of the current building and calculate the free capacity for it.
						int buildingResourceCapacity = planetBuilding.GetFreeResourceCapacity(resource.ResourceName);
						// If our storage is less full, we need the resource more than the last building
						if (buildingResourceCapacity > lastBuildingResourceCapacity)
						{
							buildingMostInNeed = planetBuilding;
						}
					}
				}
			}
			if (!excludeWarehouses && (buildingMostInNeed == null || buildingMostInNeed.GetFreeResourceCapacity(resource.ResourceName) == 0))
			{
				Tuple<PlanetWarehouse, float> closestWarehouse = null;
				foreach (PlanetEconomyBuilding planetBuilding in productionBuildings)
				{
					if (planetBuilding is PlanetWarehouse warehouse)
					{
						if (warehouse.GetFreeResourceCapacity(resource.ResourceName) == 0)
						{
							continue;
						}
						// Check if there's a path to the warehouse.
						long[] idPath = _astar.GetIdPath(fromNodeId, planetBuilding.Id);
						if (idPath.Length <= 1)
						{
							continue;
						}
						// Calculate the cost of the route (somewhat equivalent to the closest).
						float astarCost = 0;
						foreach (long pointId in idPath)
						{
							astarCost += _astar.GetPointWeightScale(pointId);
						}
						if (closestWarehouse == null)
						{
							closestWarehouse = new Tuple<PlanetWarehouse, float>(warehouse, astarCost);
							continue;
						}
						if (astarCost < closestWarehouse.Item2)
						{
							closestWarehouse = new Tuple<PlanetWarehouse, float>(warehouse, astarCost);
						}
					}
				}
				if (closestWarehouse != null)
				{
					return closestWarehouse.Item1;
				}
			}
			return buildingMostInNeed;
		}

		private List<PlanetStructure> GetPathNodes(long[] ids)
		{
			List<PlanetStructure> pathNodes = new();
			foreach (long id in ids)
			{
				PlanetStructure node = null;
				if (_buildings.ContainsKey(id))
				{
					node = _buildings[id];
				}
				else if (_transportRoutes.ContainsKey(id))
				{
					node = _transportRoutes[id];
				}
				pathNodes.Add(node);
			}
			return pathNodes;
		}

		public List<PlanetEconomyBuilding> GetEconomyBuildings()
		{
			List<PlanetEconomyBuilding> buildings = new();
			foreach (Node node in GetChildren())
			{
				if (node is PlanetEconomyBuilding productionBuilding && productionBuilding.Id != -1)
				{
					buildings.Add(productionBuilding);
				}
			}
			return buildings;
		}

		public void OnBuildingProducedResource(PlanetBuilding producingBuilding, StrategicResource resource)
		{
			if (producingBuilding.ConnectedRoutes.Count == 0)
			{
				return;
			}
			RouteResource(producingBuilding.Id, resource);
		}

		private void OnResourceInterimDestinationLost(StrategicResource resource)
		{
			long[] path = _astar.GetIdPath(resource.Path3D[0].Id, resource.Path3D[^1].Id);
			if (path.Length <= 1)
			{
				resource.IsOrphaned = true;
				OnResourceDestinationLost(resource);
				return;
			}
			else
			{
				resource.ClearPathSignals();
				resource.Path3D = GetPathNodes(path);
				foreach (PlanetStructure node in resource.Path3D)
				{
					node.Connect("Destroyed", new Callable(resource, "OnResourceInterimDestinationLost"));
				}
			}
		}

		private void OnConstructionCompleted(PlanetBuilding building)
		{
			RegisterBuilding(building);
			foreach(PlanetTransportRoute route  in building.ConnectedRoutes)
			{
				_astar.ConnectPoints(route.Id, building.Id);
			}
		}

		private void OnResourceDestinationLost(StrategicResource resource)
		{
			// Don't route the resource again if it's path length is zero, the node it's on has been destroyed
			if(resource.Path3D.Count > 0)
			{
				RouteResource(resource.Path3D[0].Id, resource);
			}
		}

		private void OnStructureDestroyed(PlanetStructure structure)
		{
			_astar.RemovePoint(structure.Id);
			switch(structure)
			{
				case PlanetBuilding building:
					_buildings.Remove(building.Id);
					break;
				case PlanetTransportRoute route:
					_transportRoutes.Remove(route.Id);
					break;
			}
			structure.Id = -1;
		}
	}
}

