using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Runtime.Intrinsics.X86;
using Entities;
using Godot;

namespace Entities
{
    public partial class Fleet : CharacterBody3D
    {
        [Export]
        private float _maxVelocity = 50.0f;
        [Export]
        private float _acceleration = 1.0f;
        [Export]
        private float _angularVelocity = (float) Math.PI / 2;
        private float _currentVelocity = 0.0f;
        private Vector3 _startPosition;
        private Vector3 _targetPosition;
        private double _movementOffset;
        private bool _isMoving;
        private Tween _rotationTween;
        bool _lookDirection = true;
        public Curve3D MovementCurve { get; } = new();
        public Vector3 TargetPosition {
            get { return _targetPosition; }
            set
            {
                _targetPosition = value;
                StartMovement();
            }
        }

        public override void _Ready()
        {
            AddToGroup("Fleets");
        }

        public override void _PhysicsProcess(double delta)
        {
            base._PhysicsProcess(delta);
            if (MovementCurve.PointCount > 0 && _isMoving)
            {
                float curveLength = MovementCurve.GetBakedLength();
                float acceleration = (float) (_acceleration * delta);
                if (_movementOffset >= curveLength * 0.5)
                {
                    if (_lookDirection)
                    {
                        Transform3D newTransform = GlobalTransform.Rotated(Vector3.Up, (float) Math.PI);
                        float angle = GlobalBasis.Z.AngleTo(-GlobalBasis.Z);
                        _rotationTween = GetTree().CreateTween();
                        _rotationTween.TweenProperty(this, "quaternion", newTransform.Basis.GetRotationQuaternion(), angle / _angularVelocity);
                    }
                    acceleration *= -1;
                    _lookDirection = false;
                }
                _currentVelocity = (float) Math.Clamp(_currentVelocity + acceleration, 0, _maxVelocity);
                _movementOffset += _currentVelocity * delta;
                if (_movementOffset <= curveLength)
                {
                    Velocity = MovementCurve.SampleBaked((float) _movementOffset) - GlobalPosition;
                    MoveAndSlide();
                    if (_rotationTween == null || !_rotationTween.IsRunning())
                    {
                        LookAt(MovementCurve.SampleBaked((float) (_movementOffset + _currentVelocity * delta)), Vector3.Up, _lookDirection);
                    }
                }
                else
                {
                    _isMoving = false;
                    _movementOffset = 0;
                    _currentVelocity = 0;
                }
            }
        }

        private void StartMovement()
        {
            _startPosition = GlobalPosition;
            BuildMovementLine();
            _lookDirection = true;
            Vector3 lookPosition = MovementCurve.SampleBaked(0.5f);
            Transform3D newTransform = GlobalTransform.LookingAt(lookPosition, Vector3.Up, true);
            float angle = GlobalBasis.Z.AngleTo(newTransform.Basis.Z);
            _rotationTween = GetTree().CreateTween();
            _rotationTween.TweenProperty(this, "global_transform", newTransform, angle / _angularVelocity).SetTrans(Tween.TransitionType.Quad);
            _rotationTween.TweenCallback(Callable.From(() => _isMoving = true)).SetDelay(0.25);
        }

        private void BuildMovementLine()
        {
            MovementCurve.ClearPoints();
            int maxI = 20;
            for (int i = 0; i <= maxI; i++)
            {
                MovementCurve.AddPoint(_startPosition.Slerp(TargetPosition, i * (1.0f / maxI)));
            }
        }
    }    
}
