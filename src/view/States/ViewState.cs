namespace View.ViewStates
{
    using Godot;

    public abstract partial class ViewState : Node
    {
        [Export]
        public ViewController ViewController;
        [Export]
        protected float _mouseSensitivity = 0.0075f;

        [Export]
        protected float _zoomSensitivity = 0.75f;

        [Export]
        protected float _maximumPitch = Mathf.DegToRad(45.0f);

        [Export]
        protected float _minimumPitch = Mathf.DegToRad(-45.0f);

        protected float _minimumFov = 45.0f;
        protected float _maximumFov = 145.0f;
        protected float _defaultFov;
        public Camera3D ViewCamera { get; set; }
        public bool Active { get; set; }

        public abstract void Enter(ViewState previousState);
        public abstract void Exit();
    }
}
