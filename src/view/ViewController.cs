namespace View
{
    using System.Linq;
    using Godot;
    using View.ViewStates;

    public partial class ViewController : Node
    {
        [Export]
        public float TransitionDuration = 1.0f;
        [Export]
        private ViewState _activeViewState;

        public ViewState ActiveViewState
        {
            get { return _activeViewState; }
            set
            {
                if (_activeViewState != null)
                {
                    _activeViewState.Exit();
                    _activeViewState.Active = false;
                }
                value.Active = true;
                value.Enter(_activeViewState);
                _activeViewState = value;
            }
        }

        public override void _Ready()
        {
            Camera3D camera = GetNodeOrNull<Camera3D>("CameraPivot/Camera3D");
            Node states = GetNodeOrNull<Node>("States");
            if (states != null)
            {
                foreach (ViewState state in states.GetChildren().Cast<ViewState>())
                {
                    state.ViewCamera = camera;
                }
                ActiveViewState = GetNodeOrNull<ViewState>("States/SystemViewState");
            }
        }
    }
}
