namespace View.PlanetView.Buildings.Ui
{
    using Godot;
    using View.PlanetView.Structures;

    public partial class PlanetConstructionBuildingUi : PlanetEconomyBuildingUi
    {
        private HBoxContainer _workLabel;

        protected override void BuildUi(bool firstBuild)
        {
            PlanetConstructionBuilding constructionBuilding = (PlanetConstructionBuilding) _structure;
            PlanetConstructionBuildingUiConfig uiConfig = (PlanetConstructionBuildingUiConfig) constructionBuilding.UiConfig;

            base.BuildUi(firstBuild);
            if(firstBuild)
            {
                ClearLabels();
                _workLabel = (HBoxContainer) _resourceTemplate.Duplicate();
                _workLabel.Visible = true;
                foreach(HBoxContainer node in _outputResourceContainer.GetChildren())
                {
                    node.QueueFree();
                }
                _outputResourceContainer.AddChild(_workLabel);
                _workLabel.GetChild<Label>(0).Text = uiConfig.WorkDoneLabelText;
            }
            string workAmount = constructionBuilding.WorkDone.ToString();
            workAmount += "/" + constructionBuilding.WorkRequired.ToString();
            _workLabel.GetChild<Label>(1).Text = workAmount;
        }
    }
}

