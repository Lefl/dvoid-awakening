namespace View.PlanetView.Buildings.Ui
{
	using Godot;
	using StrategicResources;
	using View.PlanetView.Structures;
	using System.Collections.Generic;

	public partial class PlanetTransportHubUi : PlanetEconomyBuildingUi
	{
		private readonly Dictionary<StrategicResource, HBoxContainer> _transportedResourcelabels = new();
		public override void _Ready()
		{
			base._Ready();
		}

		protected override void BuildUi(bool firstBuild)
		{
			PlanetTransportHub hub = (PlanetTransportHub) _structure;
			PlanetEconomyBuildingUiConfig uiConfig = (PlanetEconomyBuildingUiConfig) hub.UiConfig;

			base.BuildUi(firstBuild);
			// Remove old labels
			List<StrategicResource> removeKeys = new();
			foreach(StrategicResource resource in _transportedResourcelabels.Keys)
			{
				if(!hub.TransportedResources.Contains(resource))
				{
					_transportedResourcelabels[resource].QueueFree();
					removeKeys.Add(resource);
				}
			}
			foreach(StrategicResource resource in removeKeys)
			{
					_transportedResourcelabels.Remove(resource);
			}
			// Add new labels
			foreach(StrategicResource resource in hub.TransportedResources)
			{
				if(!_transportedResourcelabels.ContainsKey(resource))
				{
					_transportedResourcelabels[resource] = prepareResourceLabel(resource);
					_inputResourceContainer.AddChild(_transportedResourcelabels[resource]);
				}
			}
		}
	}
}

