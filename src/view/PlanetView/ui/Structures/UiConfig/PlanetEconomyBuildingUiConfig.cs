namespace View.PlanetView.Buildings.Ui
{
    using Godot;

    public partial class PlanetEconomyBuildingUiConfig : PlanetStructureUiConfig
    {
        // Some Economy Buildings might ignore this and ShowOutput
        [Export]
        public bool ShowInput = true;
        [Export]
        public string InputLabelText = "Input";

        [Export]
        public bool ShowOutput = true;

        [Export]
        public string OutputLabelText = "Output";

        [Export]
        public bool HideEmptyResources = false;
    }
}

