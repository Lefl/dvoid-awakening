namespace View.PlanetView.Buildings.Ui
{
    using Godot;

    public partial class PlanetConstructionBuildingUiConfig : PlanetEconomyBuildingUiConfig
    {
        [Export]
        public string WorkDoneLabelText = "Work performed: ";
    }
}

