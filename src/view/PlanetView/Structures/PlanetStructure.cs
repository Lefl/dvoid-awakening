namespace View.PlanetView.Structures
{
    using Godot;
    using View.PlanetView.Buildings.Ui;
    using StrategicResources;
    public abstract partial class PlanetStructure : StaticBody3D
    {
        [Signal]
        public delegate void DestroyedEventHandler(PlanetStructure route);

        [Export]
        public string HumanReadableName { get; private set; } = "";

        [Export]
        public PlanetStructureUiConfig UiConfig = null;

        private int _readyCount = 0; // I don't trust "_Ready inheritance", especially in C# it seems flawed.
        public int Id { get; set; } = -1;

        public abstract void Build();

        public override void _Ready()
        {
            SetProcess(false);
            _readyCount += 1;
            if (_readyCount > 1)
            {
                GD.Print($"Ready has been called {_readyCount} times??");
            }
        }

        public virtual void Destroy()
        {
            EmitSignal("Destroyed", this);
            QueueFree();
        }
    }
}