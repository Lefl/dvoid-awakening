namespace View.PlanetView.Structures
{
    using Godot;
    using StrategicResources;
    using PlanetView.Buildings.ProductionRecipes;

    public partial class PlanetConstructionBuilding : PlanetEconomyBuilding
    {
        [Signal]
        public delegate void ConstructionCompletedEventHandler(PlanetBuilding building);
        public int WorkRequired { get; private set; } = 0;
        public int WorkDone { get; private set; } = 0; // public for UI info
        public PlanetBuilding ConstructedBuilding;

        public override void OnTick()
        {
            int _availableResourceAmount = 0;
            foreach (StrategicResource recipeResource in ConstructedBuilding.ConstructionRecipe.InputResources)
            {
                _availableResourceAmount += StoredInputResources[recipeResource.ResourceName].Amount;
            }
            if (WorkDone < _availableResourceAmount)
            {
                WorkDone += 1;
                if (WorkDone == WorkRequired)
                {
                    ConstructBuilding();
                }
            }
        }

        public override void Build()
        {
            Build(ConstructedBuilding);
        }

        public void Build(PlanetBuilding building)
        {
            ConstructedBuilding = building;
            ConstructedBuilding.Visible = false;
            ConstructedBuilding.GetNode<CollisionShape3D>("CollisionShape3D").Disabled = true;
            foreach (StrategicResource resource in ConstructedBuilding.ConstructionRecipe.InputResources)
            {
                WorkRequired += resource.Amount;
                _storedInputResources[resource.ResourceName] = (StrategicResource) resource.Duplicate();
                _storedInputResources[resource.ResourceName].Amount = 0;
            }
            base.Build();
        }

        public void ConstructBuilding()
        {
            ConstructedBuilding.Visible = true;
            ConstructedBuilding.GetNode<CollisionShape3D>("CollisionShape3D").Disabled = true;
            foreach (PlanetTransportRoute route in ConnectedRoutes)
            {
                if (route.Start == this)
                {
                    route.Start = ConstructedBuilding;
                }
                else
                {
                    route.Ending = ConstructedBuilding;
                }
                ConstructedBuilding.AddRouteConnection(route);
            }
            EmitSignal("ConstructionCompleted", ConstructedBuilding);
            ConstructedBuilding.GetNode<CollisionShape3D>("CollisionShape3D").Disabled = false;
            ConstructedBuilding.Build();
            // Can't use Destroy() since that would free the transport routes so we basically reimplement toplevel Destroy here.
            EmitSignal("Destroyed", this);
            QueueFree();
        }

        public override int GetFreeResourceCapacity(string resourceName)
        {
            foreach (StrategicResource resource in ConstructedBuilding.ConstructionRecipe.InputResources)
            {
                if (resource.ResourceName == resourceName)
                {
                    return resource.Amount - _storedInputResources[resourceName].Amount;
                }
            }
            return 0;
        }
    }
}
