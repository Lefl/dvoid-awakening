namespace View.PlanetView.Structures
{
    using Godot;
    using System.Collections.Generic;
    using StrategicResources;

    public partial class PlanetExtractionBuilding : PlanetEconomyBuilding
    {
        [Export]
        private StrategicResource[] _extractedResourcesPerTick = null;

        public override void _Ready()
        {
            base._Ready();
            InputStorageSize = 0;
            foreach (StrategicResource resource in _extractedResourcesPerTick)
            {
                StrategicResource outputResource = (StrategicResource) resource.Duplicate();
                outputResource.Amount = 0;
                _storedOutputResources.Add(outputResource.ResourceName, outputResource);
            }
        }

        public override void OnTick()
        {
            foreach(StrategicResource resource in _extractedResourcesPerTick)
            {
                int newAmount = Mathf.Min(_storedOutputResources[resource.ResourceName].Amount + resource.Amount, OutputStorageSize);
                _storedOutputResources[resource.ResourceName].Amount = newAmount;
                EmitSignal("ProducedResource", this, _storedOutputResources[resource.ResourceName]);
            }
        }

        public override void ReceiveResource(StrategicResource resource)
        {
            return;
        }
    }
}
