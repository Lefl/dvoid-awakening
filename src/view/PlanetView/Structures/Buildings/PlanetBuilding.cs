namespace View.PlanetView.Structures
{
	using Godot;
	using System.Collections.Generic;
	using System.Diagnostics;
	using View.PlanetView.Buildings.ProductionRecipes;

	public abstract partial class PlanetBuilding : PlanetStructure
	{
		[Export]
		protected Color _validColor = new Color();
		[Export]
		protected Color _invalidColor = new Color();
		[Export]
		public int MaximumConnectedRoutes = 1;
		[Export]
		protected float _tickLength = 1.0f;
		[Export]
		public ConstructionRecipe ConstructionRecipe { get; protected set;}

		protected double _tickTimer = 0.0f;
		public List<PlanetTransportRoute> ConnectedRoutes { get; private set; } = new List<PlanetTransportRoute>();
		public bool ValidPlacement { get; private set; } = true;

		public abstract void OnTick();

		public override void _Ready()
		{
			CollisionLayer = 0b10;
			AddToGroup("PlanetBuilding");
			base._Ready();
		}

		public override void _Process(double delta)
		{
			_tickTimer += delta;
			if (_tickTimer >= _tickLength)
			{
				OnTick();
				_tickTimer = 0.0f;
			}
		}

		public override void Build()
		{
			SetProcess(true);
		}

		public bool HasRouteConnection(PlanetTransportRoute route)
		{
			return ConnectedRoutes.Contains(route);
		}

		public void AddRouteConnection(PlanetTransportRoute route)
		{
			if (ConnectedRoutes.Count < MaximumConnectedRoutes || MaximumConnectedRoutes == -1)
			{
				ConnectedRoutes.Add(route);
			}
		}

		public void RemoveRouteConnection(PlanetTransportRoute route)
		{
			ConnectedRoutes.Remove(route);
		}

		protected void OnAreaBodyEntered(PhysicsBody3D body)
		{
			if (body is PlanetBuilding && body != this)
			{
				ValidPlacement = false;
				MeshInstance3D plate = GetNode<MeshInstance3D>("CollisionShape3D/Plate");
				if (plate != null) {
					StandardMaterial3D material = (StandardMaterial3D) plate.GetSurfaceOverrideMaterial(0);
					material.AlbedoColor = _invalidColor;
					material.Emission = _invalidColor;
				}
			}
		}
		protected void OnAreaBodyExited(PhysicsBody3D body)
		{
			Area3D area = GetNode<Area3D>("Area3D");
			ValidPlacement = true;
			foreach (PhysicsBody3D overlappingBody in area.GetOverlappingBodies())
			{
				if (overlappingBody is PlanetBuilding && overlappingBody != this)
				{
					ValidPlacement = false;
					break;
				}
			}
			MeshInstance3D plate = GetNode<MeshInstance3D>("CollisionShape3D/Plate");
			if (plate != null) {
				StandardMaterial3D material = (StandardMaterial3D) plate.GetSurfaceOverrideMaterial(0);
				material.AlbedoColor = ValidPlacement ? _validColor : _invalidColor;
				material.Emission = ValidPlacement ? _validColor : _invalidColor;
			}
		}

		public override void Destroy()
		{
			foreach (PlanetTransportRoute route in ConnectedRoutes)
			{
				route.Destroy();
			}
			base.Destroy();
		}
	}
}






