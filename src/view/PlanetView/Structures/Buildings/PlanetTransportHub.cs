namespace View.PlanetView.Structures
{
    using Godot;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using StrategicResources;

    public partial class PlanetTransportHub : PlanetEconomyBuilding
    {
        // Must be a list, therefore we're not using InputResources
        public List<StrategicResource> TransportedResources { get; private set; }= new List<StrategicResource>();

        public override void OnTick()
        {
            for (int i = TransportedResources.Count - 1; i >= 0; i--)
            {
                StrategicResource resource = TransportedResources[i];
                if (resource.Path3D[1] is PlanetTransportRoute route && route.FreeCapacity > 0)
                {
                    resource.Path3D.RemoveAt(0);
                    TransportedResources.RemoveAt(i);
                    route.ReceiveResource(resource);
                }
            }
        }

        public override void ReceiveResource(StrategicResource resource)
        {
            TransportedResources.Add(resource);
        }
    }
}
