namespace View.PlanetView.Structures
{
    using StrategicResources;
    public partial class PlanetWarehouse : PlanetEconomyBuilding
    {
        public override void _Ready()
        {
            OutputStorageSize = 0;
            base._Ready();
        }
        public override void OnTick()
        {
            foreach (StrategicResource resource in _storedInputResources.Values)
            {
                if (resource.Amount != 0)
                {
                    EmitSignal("ProducedResource", this, resource);
                }
            }
        }
    }
}
