namespace View.PlanetView.Buildings.ProductionRecipes
{
	using Godot;
	using StrategicResources;
	using System.Collections.Generic;
	public partial class ConstructionRecipe : Resource
	{
		[Export]
		public StrategicResource[] InputResources { get; private set; }
	}
}
